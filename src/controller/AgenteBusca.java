package controller;

import java.util.LinkedList;
import java.util.List;

import model.Labirinto;
import model.Solucao;

/*
 * Agente de Busca
 * Esta classe visa encapsular m�todos e opera��o que operam sobre um ambiente totalmente observ�vel, o LABIRINTO,
 * para descobrir o melhor caminho entre o ESTADO_INICIAL e ESTADO_FINAL.
 * Para facilitar o uso da mesma como interface do presente estado da busca, a inst�ncia conta com algumas v�riaveis
 * de execu��o, como:
 * atual ~> ponteiro para solu��o atual
 * fronteira ~> lista de fronteira de solu��es a Serem analisadas
 * visitados ~> lista de CELULAS X, Y do labirinto j� visitadas
 * sleepTime ~> tempo em milisegundos que o algoritmo dorme antes de continuar a analise.
 * 				Esta v�riavel � �til para animar o processo de busca, possibilitando a 
 * 				visualiza��o do processo de busca.
 */

public class AgenteBusca {
	
	/*
	 * BUSCA
	 * Define tr�s tipos de Busca que podem ser executadas para Buscar a solu��o:
	 * A Estrela,
	 * Gulosa,
	 * Profundidade e
	 * Amplitude
	 */
	public static enum Busca{ AESTRELA, GULOSA, PROFUNDIDADE, AMPLITUDE };
	
	/*
	 * V�riaveis de inst�ncia
	 * 
	 */
	private Labirinto 	ambiente;
	private int[] 		estadoInicial;
	private int[] 		estadoObjetivo;
	
	private Solucao 		atual;
	private List<Solucao> 	fronteira;
	private List<int[]> 	visitados;
	private long 			sleepTime;
	private Busca			tipoBusca;		
	
	
	public AgenteBusca(int[] estadoInicial, int[] estadoObjetivo, Labirinto labirinto) {
		this.estadoInicial = estadoInicial;
		this.estadoObjetivo = estadoObjetivo;
		this.ambiente = labirinto;
		this.tipoBusca = Busca.AESTRELA;
		
		this.sleepTime = 200;
		
		this.resetarBusca();
	}
	
	public AgenteBusca(Labirinto labirinto) {
		this(labirinto.getCelulaAleatoria(), labirinto.getCelulaAleatoria(), labirinto);
	} 
	
	/*
	 * Resetar Busca
	 * Este m�todo reinst�ncia as vari�veis de execu��o para iniciar um processo de busca
	 */
	
	public void resetarBusca() {
		this.atual = new Solucao(estadoInicial[0], estadoInicial[1]);
		
		this.fronteira = new LinkedList<Solucao>();
		this.fronteira.add(atual);
		
		this.visitados = new LinkedList<int[]>();
		
		this.ambiente.limparVisitas();
	}
	
	/*
	 * Processos de Busca
	 * O algoritmo de Busca generico foi implementado de maneira que dependendo do tipo de Busca
	 * definido o algoritmo busca de maneira diferente a proxima solu��o a ser visitada.
	 * Para montar uma interface entre a busca, ele oferece 2 m�todos:
	 * 
	 * iniciarBusca
	 * Que reseta as v�riaveis de execu��o para iniciar um processo de busca do 0
	 * 
	 * continuarBusca
	 * Este m�todo continua a execu��o de uma busca a partir do estado das v�riaveis de inst�ncia atual
	 * 
	 * Ambos os m�todos retorna uma SOL��O aceit�vel (que atinge o ESTADO OBJETIVO) poss�vel ou NULO
	 * caso esgote a busca e n�o encontre outra solu��o
	 */
	
	public Solucao iniciarBusca(){
		this.resetarBusca();
		return this.Busca();
	}
	
	public Solucao continuarBusca(){
		return this.Busca();
	}
	
	/*
	 * Busca
	 * Implementação gen�rica de um algoritmo de Busca, que segue os seguintes passos:
	 * 
	 * 1 - fa�a ATUAL = seleciona e retira proxima visita da Fronteira
	 * 2 - Se foi visitado então volte para 1
	 * 3 - Se ATUAL é uma solução que está no ESTADO OBJETIVO então retorna ATUAL
	 * 4 - Adiciona estados filhos de ATUAL a FRONTEIRA
	 * 5 - Repete 1 a 5 enquanto a FRONTEIRA não está VAZIA
	 * 6 - Retorna NULO
	 */
	public Solucao Busca(){
		while(!fronteira.isEmpty()) {
			this.atual = proximaVisita();
			
			if(!foiVisitada(this.atual)) {
				if(this.isObjetivo(atual)){
					System.out.print("Encontrada Solucao");
					return atual;
				} else {
					this.visitarEstado();
					//Aproveitando que N, S, L e O tem os valores 0, 1, 2 e 3
					for(int i = 0; i < 4; i++) {
						if(this.getAmbiente().movimentoValido(atual.getX(), atual.getY(), i))
							this.fronteira.add(this.getAtual().moverPara(i));
					}
				}
				//SLEEP de EXECU��O
				try { Thread.sleep(this.sleepTime);} catch (InterruptedException e) {}
			}
		}
		System.out.print("Nenhuma Solucao Encontrada");
		return null;
	}
	
	/*
	 * proximaVisita
	 * Este método seleciona e retira a proxima solução a ser visitada da FRONTEIRA de acordo com o
	 * algoritmo de Busca. Ele percore a fronteira procurando o nodo que melhor atende a seleção do
	 * tipo de Busca escolhida.
	 * 
	 * Seleções:
	 * A ESTRELA ~> seleciona solução com menor F(), ou seja, menor soma entre Heuristica e Custo
	 * PROFUNDIDADE ~> seleciona a solução de maior custo, pois neste caso o custo representa o número de visitas
	 * AMPLITUDE ~> seleciona a solução com menor custo
	 * GULOSA ~> seleciona a solução de menor Heurística
	 */
	private Solucao proximaVisita() {
		int melhor = 0;
		for(int i = 1; i < this.fronteira.size(); i++) {
			switch(this.getTipoBusca()) {
			case AESTRELA:
				if(this.funcaoF(fronteira.get(melhor)) > this.funcaoF(fronteira.get(i)))
					melhor = i;
				break;
			case GULOSA:
				if(this.funcaoHeuristica(fronteira.get(melhor)) > this.funcaoHeuristica(fronteira.get(i)))
					melhor = i;
				break;
			case AMPLITUDE:
				if(this.funcaoCusto(fronteira.get(melhor)) > this.funcaoCusto(fronteira.get(i)))
					melhor = i;
				break;
			case PROFUNDIDADE:
				if(this.funcaoCusto(fronteira.get(melhor)) < this.funcaoCusto(fronteira.get(i)))
					melhor = i;
				break;
			}
		}
		return fronteira.remove(melhor);
	}
	
	/*
	 * foi Visitada
	 * Este método encapsula a verificação se uma CELULA X, Y já foi visitada pelo algoritmo de busca, ou seja,
	 * se ela está contida na lista de VISITADAS
	 */
	
	private boolean foiVisitada(Solucao solucao) {
		for(int i = 0; i < this.visitados.size(); i++) {
			if(this.visitados.get(i)[0] == solucao.getX() && this.visitados.get(i)[1] == solucao.getY())
				return true;
		}
		return false;
	}
	
	/*
	 * is Objetivo
	 * Este método encapsula a verificação se a Solucao S está no ESTADO OBJETIVO
	 */
	private boolean isObjetivo(Solucao solucao){
		return solucao.getX() == this.estadoObjetivo[0] && solucao.getY() == this.estadoObjetivo[1];
	}
	
	/*
	 * Funções de Custo
	 * Estas funções encapsulam o calculo para as funções de custo de uma dada solução.
	 * 
	 * Função F
	 * Resulta o custo total da solução mais a heurística da solução. Representa, portanto, uma estimativa
	 * do custo total da solução.
	 * 
	 * Função Custo
	 * Resulta no custo acumulado dos passos tomados desde da origem até o estado atual da solução
	 * 
	 * Função Heuristíca
	 * Estimativa do custo da posição de uma solução até o Objetivo. No caso do labirinto, uma boa heurística pode
	 * ser a distância linear entre a posição da solução S até o OBJETIVO
	 */
	private double funcaoF(Solucao solucao){
		return funcaoCusto(solucao) + funcaoHeuristica(solucao);
	}
	
	private double funcaoCusto(Solucao solucao) {
		return solucao.getCusto();
	}
	
	private double funcaoHeuristica(Solucao solucao){
		double distancia = Math.pow(solucao.getX() - this.estadoObjetivo[0], 2);
		distancia += Math.pow(solucao.getY() - this.estadoObjetivo[1], 2);
		return Math.sqrt(distancia);
	}
	
	/*
	 * Visitar Estado
	 * Método que encapsula a visita de uma posição do labirinto, adicionando a posição X, Y
	 * a lista de visitados e avisa ao ambiente que a CELULA X, Y foi visitada.
	 */
	
	private void visitarEstado(){
		int[] posicao = {this.getAtual().getX(), this.getAtual().getY()};
		this.visitados.add(posicao);
		this.ambiente.getCelula(this.getAtual().getX(), this.getAtual().getY()).setVisitada(true);
	}

	/*
	 * Getter e Setters da váriaveis de instância
	 */
	public Labirinto getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(Labirinto ambiente) {
		this.ambiente = ambiente;
		this.estadoInicial = ambiente.getCelulaAleatoria();
		this.estadoObjetivo = ambiente.getCelulaAleatoria();
		
		this.resetarBusca();
	}

	public int[] getEstadoInicial() {
		return estadoInicial;
	}

	public void setEstadoInicial(int[] estadoInicial) {
		this.estadoInicial = estadoInicial;
		this.resetarBusca();
	}

	public int[] getEstadoObjetivo() {
		return estadoObjetivo;
	}

	public void setEstadoObjetivo(int[] estadoObjetivo) {
		this.estadoObjetivo = estadoObjetivo;
		this.resetarBusca();
	}

	public Solucao getAtual() {
		return atual;
	}

	public List<Solucao> getFronteira() {
		return fronteira;
	}

	public List<int[]> getVisitados() {
		return visitados;
	}

	public Busca getTipoBusca() {
		return tipoBusca;
	}

	public void setTipoBusca(Busca tipoBusca) {
		this.tipoBusca = tipoBusca;
	}

    public long getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }
        
}
