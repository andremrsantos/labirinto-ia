package model;

/**
 * @author André M Ribeiro dos Santos
 * @version 0.2
 * 
 * Feito em 1/11/2011
 * Ultima modificação 08/11/2011
 * 
 * Celula
 * Representação de unidade do Labirinto
 * com 4 váriaveis booleanas, 4 representando a presença das paredes Norte,
 * Sul, Leste e Oeste e 1 representando se a célula já voi visitada.
 * 
 */
public class Celula {

    private boolean paredeNorte, paredeSul, paredeLeste, paredeOeste;
    private boolean visitada;

    public Celula(boolean fechada) {
        this.paredeLeste = fechada;
        this.paredeOeste = fechada;
        this.paredeNorte = fechada;
        this.paredeSul = fechada;
        this.visitada = false;
    }

    public Celula(){
        this(true);
    }
    
    /*
     * Getter e Setter para a variavel visitada
     * 
     */
    public boolean isVisitada() {
        return visitada;
    }
    
    public void setVisitada(boolean visitada) {
        this.visitada = visitada;
    }
    
    /*
     * Encapsulamento da destruição, criação e alteração das paredes
     * 
     * destruirParede, contruirParede e alterarParede
     * recebem o sentido N, S, L e O e 
     * altera o estado da parede para verdadeiro ou falso,
     * segundo o método
     * 
     * destruirParede   ~> muda o estado para falso
     * construirParede  ~> muda o estado para verdadeiro
     * alterarParede    ~> muda o estado para ¬estado
     */
    
    public void destruirParede(int sentido) {
        switch (sentido) {
            case Labirinto.N:
                this.paredeNorte    = false;
                break;
            case Labirinto.S:
                this.paredeSul      = false;
                break;
            case Labirinto.O:
                this.paredeOeste    = false;
                break;
            case Labirinto.L:
                this.paredeLeste    = false;
                break;
        }
    }
    
    public void construirParede(int sentido) {
        switch (sentido) {
            case Labirinto.N:
                this.paredeNorte    = true;
                break;
            case Labirinto.S:
                this.paredeSul      = true;
                break;
            case Labirinto.O:
                this.paredeOeste    = true;
                break;
            case Labirinto.L:
                this.paredeLeste    = true;
                break;
        }
    }
    
    public void alterarParede(int sentido) {
        switch (sentido) {
            case Labirinto.N:
                this.paredeNorte    = !this.paredeNorte;
                break;
            case Labirinto.S:
                this.paredeSul      = !this.paredeSul;
                break;
            case Labirinto.O:
                this.paredeOeste    = !this.paredeOeste;
                break;
            case Labirinto.L:
                this.paredeLeste    = !this.paredeLeste;
                break;
        }
    }
    /*
     * Getters das Paredes
     */

    public boolean isParedeLeste() {
        return paredeLeste;
    }

    public boolean isParedeNorte() {
        return paredeNorte;
    }

    public boolean isParedeOeste() {
        return paredeOeste;
    }

    public boolean isParedeSul() {
        return paredeSul;
    }
 
}

