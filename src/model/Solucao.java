package model;

import java.util.ArrayList;

/*
 * Solucao
 * Classe para representar uma solu��o a ser analisada pelo Agente de Busca
 * Ela conta com dois inteiros X, Y para representar a posi��o da solu��o no labirinto,
 * um inteiro para guardar o custo da solu��o, um inteiro para guardar o sentido do movimento
 * que gerou a solu��o atual e um ponteiro para a solu��o pai, ou seja,
 * a solu��o de onde andando em algum sentido gerou a solu��o atual
 */

public class Solucao {
	/*
	 * V�riaveis de Inst�ncia
	 * 
	 * x, y ~> posi��o da solu��o no labirinto
	 * custo ~> custo da solu��o
	 * pai ~> ponteiro para solu��o pai
	 * acao ~> sentido do movimento que do n� pai gerou a solu��o atual
	 */
	private int x, y;
	private int custo;
	private Solucao pai;
	private int acao;
	
	public Solucao(int x, int y, int custo, int acao, Solucao pai) {
		this.x = x;
		this.y = y;
		this.pai = pai;
		this.acao = acao;
		this.custo = custo;
	}
	
	public Solucao(int x, int y){
		this(x, y, 0, -1, null);
	}
	
	/*
	 * Getter das v�riaveis de inst�ncia
	 */
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getCusto() {
		return custo;
	}

	public Solucao getPai() {
		return pai;
	}

	public int getAcao() {
		return acao;
	}
	
	/*
	 * getCaminho
	 * Retorna um vetor da sequ�ncia de posi��es X, Y que gerou a solu��o atual
	 */
	
	public int[][] getCaminho() {
		ArrayList<int[]> caminho = new ArrayList<int[]>();
		Solucao atual = this;
		while(atual != null){
			int[] pos = {atual.getX(), atual.getY()};
			caminho.add(pos);
			atual = atual.getPai();
		}
		return caminho.toArray(new int[1][1]);
	}
	
	/*
	 * getMovimentos
	 * Retorna um vetor de a��es que gera da origem a solu��o atual
	 */
	
	public Integer[] getMovimentos() {
		ArrayList<Integer> movimentos = new ArrayList<Integer>();
		Solucao atual = this;
		while(atual.getPai() != null){
			movimentos.add(atual.getAcao());
			atual = atual.getPai();
		}
		return movimentos.toArray(new Integer[1]);
	}
	
	/*
	 * moverPara
	 * Retorna uma nova solu��o filha da atual fruta da a��o
	 */
	
	public Solucao moverPara(int acao) {
		return new Solucao(
				this.getX() + Labirinto.DX[acao], 
				this.getY() + Labirinto.DY[acao], 
				this.getCusto() + 1, acao, this);
	}
}
